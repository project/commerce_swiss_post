<?php

namespace Drupal\commerce_swiss_post\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\FlatRate;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the FlatRate shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "swiss_post",
 *   label = @Translation("Swiss Post"),
 * )
 */
class SwissPost extends FlatRate implements SupportsTrackingInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'shipping_method' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shipping_method'] = [
      '#type' => 'textfield',
      '#title' => t('Swiss Post Shipping Method (ECO, PRI, ..)'),
      '#default_value' => $this->configuration['shipping_method'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['shipping_method'] = $values['shipping_method'];
    }
  }

  /**
   * Returns a tracking URL for UPS shipments.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The commerce shipment.
   *
   * @return mixed
   *   The URL object or FALSE.
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $code = $shipment->getTrackingCode();

    if (!empty($code)) {
      return Url::fromUri('https://www.post.ch/swisspost-tracking', ['query' => ['formattedParcelCodes' => $code]]);
    }
    return FALSE;
  }


}
