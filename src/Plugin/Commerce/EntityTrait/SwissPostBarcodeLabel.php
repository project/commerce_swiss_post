<?php

namespace Drupal\commerce_swiss_post\Plugin\Commerce\EntityTrait;

use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the "commerce_swiss_post_barcode_label" trait.
 *
 * @CommerceEntityTrait(
 *   id = "commerce_swiss_post_barcode_label",
 *   label = @Translation("Allows to store the received Swiss Post barcode labels"),
 *   entity_types = {"commerce_shipment"}
 * )
 */
class SwissPostBarcodeLabel extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];
    $fields['commerce_swisspost_barcode_label'] = BundleFieldDefinition::create('file')
      ->setDescription(t('If this field is left empty, the price of the product will be used as the giftcard amount'))
      ->setLabel(t('Swiss Post Barcode Label'))
      ->setSettings([
        // @todo: Requires private scheme to be enabled, check?
        // @todo: Field vs. storage settings?
        'uri_scheme' => 'private',
        'file_extensions' => 'pdf',
        'file_directory' => 'swiss_post_label/[date:custom:Y]-[date:custom:m]'
      ])
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'file_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
