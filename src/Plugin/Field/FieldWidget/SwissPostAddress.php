<?php

namespace Drupal\commerce_swiss_post\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\address\Plugin\Field\FieldWidget\AddressDefaultWidget;

/**
 * Plugin implementation of the 'commerce_swiss_post_address' widget.
 *
 * @FieldWidget(
 *   id = "commerce_swiss_post_address",
 *   label = @Translation("Address with Swiss Post validation"),
 *   field_types = {
 *     "address"
 *   }
 * )
 */
class SwissPostAddress extends AddressDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#element_validate'][] = [static::class, 'swissPostAddressValidate'];
    return $element;
  }

  /**
   * Validation callback for Swiss addresses via Swiss Post.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $complete_form
   *   The whole form.
   */
  public static function swissPostAddressValidate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $parents = $element['#array_parents'];
    $config = \Drupal::configFactory()->get('commerce_swiss_post.settings');
    array_splice($parents, -2);

    $address_values = $form_state->getValue($parents);
    foreach ($address_values as $delta => $value) {
      if (!isset($value['address'])) {
        return;
      }
      $address = $value['address'];

      if (!empty($address['address_line1']) && !empty($address['postal_code']) && !empty($address['locality'])) {
        // If the data is the same as the previous submission, skip the
        // verification.
        if (isset($_SESSION['commerce_swiss_post_previous_input']) && $address == $_SESSION['commerce_swiss_post_previous_input']) {
          if ($config->get('aws_log_level') >= 2) {
            \Drupal::logger('commerce_swiss_post')->info(t('Validation skipped due to double submission. Address %original.', [
              '%original' => implode(' ', $address),
            ]));
          }
          return;
        }

        // Ignore non-CH/LI countries.
        if (!in_array($address['country_code'], ['CH', 'LI'])) {
          return;
        }

        /** @var \Drupal\commerce_swiss_post\AddressVerification $address_verification */
        $address_verification = \Drupal::service('commerce_swiss_post.address_verification');
        $corrected_address = [];

        $result = $address_verification->verifyAddress($address['address_line1'], $address['postal_code'], $address['locality'], $corrected_address);
        if (!$result) {
          $form_state->setErrorByName(implode('][', $parents) . '][address_line1', t('Your mailing address is not valid. Check the fields Street, City and Zip. If you are certain that the address is correct, re-submit with the same data.'));

          if ($form_state->getLimitValidationErrors() === NULL) {
            $_SESSION['commerce_swiss_post_previous_input'] = $address;
          }
        }
        // If validation was successful, we might have received a corrected
        // address, update the form values.
        elseif ($corrected_address) {
          $address['address_line1'] = $corrected_address['street'];
          $address['postal_code'] = $corrected_address['zip'];
          $address['locality'] = $corrected_address['town'];
          $form_state->setValue(array_merge($parents, [$delta, 'address']), $address);
        }
      }
    }
  }

}
