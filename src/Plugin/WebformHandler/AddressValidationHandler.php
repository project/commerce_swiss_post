<?php

namespace Drupal\commerce_swiss_post\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform validate handler.
 *
 * @WebformHandler(
 *   id = "commerce_swiss_post_address_validation",
 *   label = @Translation("Swiss Post address validation handler"),
 *   description = @Translation("Use Swiss Post address verification api to confirm the validity of CH and LI addresses."),
 *   category = @Translation("Settings"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class AddressValidationHandler extends WebformHandlerBase {

  use StringTranslationTrait;

  /**
   * The address verification service.
   *
   * @var \Drupal\commerce_swiss_post\AddressVerification
   */
  protected $addressVerification;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->addressVerification = $container->get('commerce_swiss_post.address_verification');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'street' => '',
      'postal_code' => '',
      'town' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $options = [];
    foreach ($fields as $field_name => $field) {
      $options[$field_name] = $field['#title'];
    }

    $form['street_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Street field'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['street_field'],
      '#weight' => 1,
    ];

    $form['postal_code_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Postal code field'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['postal_code_field'],
      '#weight' => 1,
    ];

    $form['town_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Town field'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['town_field'],
      '#weight' => 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['street_field'] = $form_state->getValue('street_field');
    $this->configuration['postal_code_field'] = $form_state->getValue('postal_code_field');
    $this->configuration['town_field'] = $form_state->getValue('town_field');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $street = $form_state->getValue($this->configuration['street_field']);
    $postal_code = $form_state->getValue($this->configuration['postal_code_field']);
    $town = $form_state->getValue($this->configuration['town_field']);

    if (!empty($street) && !empty($postal_code) && !empty($town)) {
      $address = [
        'street' => $street,
        'postal_code' => $postal_code,
        'town' => $town,
      ];

      // If the data is the same as the previous submission, skip the
      // verification.
      if (isset($_SESSION['commerce_swiss_post_previous_input']) && $address == $_SESSION['commerce_swiss_post_previous_input']) {
        return;
      }

      $corrected_address = [];
      $result = $this->addressVerification->verifyAddress($street, $postal_code, $town, $corrected_address);
      if (!$result) {
        $form_state->setError($form, $this->t('Your mailing address is not valid. Check the fields Street, City and Zip. If you are certain that the address is correct, re-submit with the same data.'));
        $_SESSION['commerce_swiss_post_previous_input'] = $address;
      }
      // If validation was successful, we might have received a corrected
      // address, update the form values.
      elseif ($corrected_address) {
        $form_state->setValue($this->configuration['street_field'], $corrected_address['street']);
        $form_state->setValue($this->configuration['postal_code_field'], $corrected_address['zip']);
        $form_state->setValue($this->configuration['town_field'], $corrected_address['town']);
      }
    }

  }

}
