<?php

namespace Drupal\commerce_swiss_post\Plugin\Action;

use Drupal\commerce_swiss_post\LabelGeneratorInterface;
use Drupal\commerce_swiss_post\Plugin\Commerce\ShippingMethod\SwissPost;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\EnforcedResponseException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Downloads the Printed entity.
 *
 * @Action(
 *   id = "commerce_swiss_post_generate_label_action",
 *   label = @Translation("Generate swiss post labels"),
 *   type = "commerce_order"
 * )
 */
class GenerateLabelAction extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * The label generator service.
   *
   * @var \Drupal\commerce_swiss_post\LabelGeneratorInterface
   */
  protected $labelGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LabelGeneratorInterface $label_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->labelGenerator = $label_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_swiss_post.label_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'administer commerce_shipment');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $orders) {
    $shipments = [];
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    foreach ($orders as $order) {
      if ($order->hasField('shipments')) {
        foreach ($order->get('shipments') as $item) {
          if ($item->entity && $item->entity->getShippingMethod()->getPlugin() instanceof SwissPost) {
            $shipments[] = $item->entity;
          }
        }
      }
    }

    try {
      $response = $this->labelGenerator->generateLabelResponse($shipments);
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('Failed to generate labels: @error', ['@error' => $e->getMessage()]));
      return;
    }
    throw new EnforcedResponseException($response);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
