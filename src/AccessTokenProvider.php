<?php

namespace Drupal\commerce_swiss_post;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Provides the Swiss Post Commerce API OAuth2 access token.
 */
class AccessTokenProvider implements AccessTokenProviderInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The expirable key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected $keyValueExpirableFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * AccessTokenProvider constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable_factory
   *   The expirable key value factory.
   * @param \Drupal\Component\Datetime\Time $time
   *   The time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, KeyValueExpirableFactoryInterface $key_value_expirable_factory, Time $time) {
    $this->configFactory = $config_factory;
    $this->keyValueExpirableFactory = $key_value_expirable_factory;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(bool $force_new = FALSE): string {
    $key_value_store = $this->keyValueExpirableFactory->get('commerce_swiss_post');
    if (!$force_new && $token = $key_value_store->get('access_token')) {
      return $token;
    }

    $config = $this->configFactory->get('commerce_swiss_post.settings');
    $provider = new GenericProvider([
      'clientId' => $config->get('client_id'),
      'clientSecret' => $config->get('client_secret'),
      'urlAuthorize' => $config->get('api_host') . '/WEDECOAuth/token',
      'urlAccessToken' => $config->get('api_host') . '/WEDECOAuth/token',
      'urlResourceOwnerDetails' => $config->get('api_host') . '/WEDECOAuth/token',
    ]);

    $token = $provider->getAccessToken('client_credentials', ['scope' => 'WEDEC_BARCODE_READ']);

    // Store the access token, subtract some time so to account for different
    // time and slower requests.
    $key_value_store->setWithExpire('access_token', $token->getToken(), $token->getExpires() - $this->time->getCurrentTime() - 10);

    return $token->getToken();
  }

}
