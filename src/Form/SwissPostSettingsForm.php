<?php

namespace Drupal\commerce_swiss_post\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class SwissPostSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_swiss_post.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_swiss_post_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_swiss_post.settings');

    if (\Drupal::moduleHandler()->moduleExists('commerce_shipping')) {

      $form['shipping'] = [
        '#type' => 'details',
        '#title' => $this->t('Shipping configuration'),
        '#open' => TRUE,
      ];

      $form['api_host'] = [
        '#type' => 'textfield',
        '#title' => $this->t('API Host'),
        '#default_value' => $config->get('api_host'),
      ];
      $form['shipping']['client_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Client ID'),
        '#default_value' => $config->get('client_id'),
      ];
      $form['shipping']['client_secret'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Client Secret Key'),
        '#default_value' => $config->get('client_secret'),
      ];
      $form['shipping']['franking_license'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Franking License'),
        '#default_value' => $config->get('franking_license'),
      ];
      $form['shipping']['label_layout'] = [
        '#type' => 'select',
        '#title' => $this->t('Label Layout'),
        '#options' => ['a5' => 'A5', 'a6' => 'A6', 'a7' => 'A7'],
        '#default_value' => $config->get('label_layout'),
      ];
      $form['shipping']['print_preview'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Print Preview'),
        '#description' => $this->t('Enables the print preview mode that will create "SPECIMEN" example labels instead of barcodes, for testing'),
        '#default_value' => $config->get('print_preview'),
      ];
    }

    $form['address_validation'] = [
      '#type' => 'details',
      '#title' => $this->t('Address web services configuration'),
      '#open' => TRUE,
    ];

    $form['address_validation']['aws_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $config->get('aws_host'),
    ];

    $form['address_validation']['aws_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('aws_username'),
    ];

    $form['address_validation']['aws_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('aws_password'),
    ];

    $form['address_validation']['aws_log_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Log level'),
      '#options' => [
        3 => $this->t('All validation checks are logged'),
        2 => $this->t('Changes and errors only'),
        1 => $this->t('Errors only'),
        0 => $this->t('None (API errors/exceptions will still be logged)'),
      ],
      '#default_value' => $config->get('aws_log_level') ?? 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('commerce_swiss_post.settings');

    if (\Drupal::moduleHandler()->moduleExists('commerce_shipping')) {
      $config
        ->set('api_host', $form_state->getValue('api_host'))
        ->set('client_id', $form_state->getValue('client_id'))
        ->set('client_secret', $form_state->getValue('client_secret'))
        ->set('franking_license', $form_state->getValue('franking_license'))
        ->set('label_layout', $form_state->getValue('label_layout'))
        ->set('print_preview', $form_state->getValue('print_preview'));
    }

    $config
      ->set('aws_host', $form_state->getValue('aws_host'))
      ->set('aws_username', $form_state->getValue('aws_username'))
      ->set('aws_password', $form_state->getValue('aws_password'))
      ->set('aws_log_level', $form_state->getValue('aws_log_level'))
      ->save();
  }

}
