<?php

namespace Drupal\commerce_swiss_post\Events;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that allows to customize the data sent to the Swiss Post Barcode API.
 */
class ShipmentToBarcodeEvent extends Event {

  /**
   * The raw barcode data sent to the Swiss Post API.
   *
   * @var array
   */
  protected $barcodeData;

  /**
   * The shipment entity.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * ShipmentToBarcodeEvent constructor.
   *
   * @param array $barcodeData
   *   The raw barcode data sent to the Swiss Post API.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   */
  public function __construct(array $barcodeData, ShipmentInterface $shipment) {
    $this->barcodeData = $barcodeData;
    $this->shipment = $shipment;
  }

  /**
   * Returns the current barcode data.
   *
   * @return array
   *   The raw barcode data sent to the Swiss Post API.
   */
  public function getBarcodeData(): array {
    return $this->barcodeData;
  }

  /**
   * Update the barcode data.
   *
   * @param array $barcode_data
   *   The raw barcode data sent to the Swiss Post API.
   */
  public function setBarcodeData(array $barcode_data): void {
    $this->barcodeData = $barcode_data;
  }

  /**
   * Returns the shipment entity.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment entity.
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

}
