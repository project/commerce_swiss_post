<?php

namespace Drupal\commerce_swiss_post;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;

/**
 * Address verification service.
 */
class AddressVerification {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected  $logger;

  /**
   * AddressVerification constructor.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   */
  public function __construct(ClientInterface $httpClient, ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->logger = $logger_factory->get('commerce_swiss_post');
  }

  /**
   * Verify an address.
   *
   * @param string $street
   *   The street name including house number.
   * @param string $zip
   *   The ZIP code.
   * @param string $town
   *   The town name.
   * @param array $corrected_address
   *   If available (status between 2 and 5), the corrected address data, with
   *   the keys street, zip and town.
   *
   * @return bool
   *   TRUE if the address is valid, verification is disabled or there was an
   *   verification error. FALSE if the verification failed.
   */
  public function verifyAddress(string $street, string $zip, string $town, array &$corrected_address = []) {
    $config = $this->configFactory->get('commerce_swiss_post.settings');
    $log_level = $config->get('aws_log_level');

    if (!$config->get('aws_host') || !$config->get('aws_username') || !$config->get('aws_password')) {
      $this->logger->error($this->t('Missing configuration for address web services.'));
      return TRUE;
    }

    try {
      $matched = static::extractStreetParts($street);
      $host = rtrim($config->get('aws_host'), '/');
      $query = [
        'StreetName' => $matched['street'],
        'HouseNo' => $matched['house_no'],
        'HouseNoAddition' => $matched['house_no_addition'],
        'ZipCode' => $zip,
        'TownName' => $town,
      ];
      $original_address = implode(' ', $query);

      $response = $this->httpClient->get($host . '/buildingverification4', [
        'query' => $query,
        'auth' => [$config->get('aws_username'), $config->get('aws_password')],
      ]);

      $json_response = Json::decode((string) $response->getBody());

      if (!isset($json_response['QueryBuildingVerification4Result']['Status']) || $json_response['QueryBuildingVerification4Result']['Status'] != 0) {
        throw new \Exception('Unexpected response: ' . (string) $response->getBody());
      }

      $status = $json_response['QueryBuildingVerification4Result']['Status'];
      $results = $json_response['QueryBuildingVerification4Result']['BuildingVerificationData'];
      $success = FALSE;
      $pstat = $results['PSTAT'] ?? -1;

      if ($log_level == 3) {
        $this->logger->info($this->t('PSTAT: %pstat. Status: %status. Address validation response: %response', [
          '%status' => $status,
          '%pstat' => $pstat,
          '%response' => json_encode($json_response),
        ]));
      }

      if ($pstat >= 1 && $pstat <= 5) {
        $success = TRUE;

        if ($pstat > 1) {
          $corrected_address = [
            'street' => $results['StreetName'] . ' ' . $results['HouseNo'] . $results['HouseNoAddition'],
            'zip' => $results['ZipCode'],
            'town' => $results['TownName'],
          ];

          if ($log_level >= 2) {
            $this->logger->info($this->t('PSTAT: %pstat. Corrected. Original: %original. Corrected: %corrected.', [
              '%pstat' => $pstat,
              '%original' => $original_address,
              '%corrected' => implode(' ', $corrected_address),
            ]));
          }
        }
        else {
          if ($log_level == 3) {
            $this->logger->info($this->t('PSTAT: %pstat. Valid. Address: %original.', [
              '%pstat' => $pstat,
              '%original' => $original_address,
            ]));
          }
        }
      }
      else {
        if ($log_level >= 1) {
          $this->logger->info($this->t('PSTAT: %pstat. Failed. Address: %original.', [
            '%pstat' => $pstat,
            '%original' => $original_address,
          ]));
        }
      }

      return $success;
    }
    catch (ServerException $e) {
      // Ignore service errors, return TRUE.
      \watchdog_exception('commerce_swiss_post', $e, 'Server Error: @error', ['@error' => (string) $e->getResponse()->getBody()]);
      return TRUE;
    }
    catch (\Exception $e) {
      // Ignore service errors, return TRUE.
      \watchdog_exception('commerce_swiss_post', $e);
      return TRUE;
    }

  }

  /**
   * Extracts house number, street number addition and street from address line.
   *
   * @param string $street_line
   *   The street line as returned from the address field.
   *
   * @return array
   *   Array containing the street, house_no and street keys.
   */
  public static function extractStreetParts(string $street_line) {
    $matched = [
      'street' => '',
      'house_no' => '',
      'house_no_addition' => '',
    ];

    // Split up the street into street, house number and addition.
    if (preg_match('/(.+) (\d*\/\d*|\d*)([a-zA-Z]*)/', $street_line, $matches)) {
      $matched['street'] = $matches[1];
      $matched['house_no'] = $matches[2];
      $matched['house_no_addition'] = $matches[3];
    }

    // Check if house number is before the street name.
    if (empty($matched['house_no']) && preg_match('/(\d*\/\d*|\d*)([a-zA-Z]*),? ?(.+)/', $street_line, $matches)) {
      $matched['street'] = $matches[3];
      $matched['house_no'] = $matches[1];
      $matched['house_no_addition'] = $matches[2];
    }

    // Sometimes we even have addresses without number. For those, just return
    // the whole street line as street.
    if (empty($matched['house_no']) && !empty($matched['house_no_addition'])) {
      $matched['street'] = $street_line;
      $matched['house_no'] = '';
      $matched['house_no_addition'] = '';
    }

    // Trim spaces, dots and commas from street.
    $matched['street'] = trim($matched['street'], " .,");

    return $matched;
  }

}
