<?php

namespace Drupal\commerce_swiss_post\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_swiss_post\LabelGeneratorInterface;
use Drupal\commerce_swiss_post\Plugin\Commerce\ShippingMethod\SwissPost;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\entity_print\PrintBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Print controller.
 */
class CommerceSwissPostLabelController extends ControllerBase {

  /**
   * The label generator.
   *
   * @var \Drupal\commerce_swiss_post\LabelGeneratorInterface
   */
  protected $labelGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(LabelGeneratorInterface $label_generator) {
    $this->labelGenerator = $label_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_swiss_post.label_generator')
    );
  }

  /**
   * Generate label for a commerce_shipment entity
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The shipment.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object on error otherwise the Print is sent.
   */
  public function printLabel(ShipmentInterface $commerce_shipment) {
    try {
      return $this->labelGenerator->generateLabelResponse([$commerce_shipment]);
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('Failed to generate labels: @error', ['@error' => $e->getMessage()]));
      return new RedirectResponse($commerce_shipment->toUrl('collection')->setAbsolute()->toString());
    }
  }

  /**
   * Verify access to the shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The shipment.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result object.
   */
  public function checkAccess(ShipmentInterface $commerce_shipment) {
    return AccessResult::allowedIf($commerce_shipment->getShippingMethod()->getPlugin() instanceof SwissPost);
  }

}
