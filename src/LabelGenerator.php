<?php

namespace Drupal\commerce_swiss_post;


use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_swiss_post\Events\ShipmentToBarcodeEvent;
use Drupal\commerce_swiss_post\Plugin\Commerce\ShippingMethod\SwissPost;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\physical\WeightUnit;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfParser\StreamReader;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * The print builder service.
 */
class LabelGenerator implements LabelGeneratorInterface {

  /**
   * @var \Drupal\commerce_swiss_post\AccessTokenProviderInterface
   */
  protected $accessTokenProvider;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new LabelGenerator.
   */
  public function __construct(AccessTokenProviderInterface $access_token_provider, ClientInterface $http_client, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher, FileSystemInterface $file_system) {
    $this->accessTokenProvider = $access_token_provider;
    $this->client = $http_client;
    $this->configFactory = $config_factory;
    $this->eventDispatcher = $event_dispatcher;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function generateLabels(array $shipment_entities) {

    $pdf_contents = [];
    foreach ($shipment_entities as $shipment_entity) {

      $shipment_plugin = $shipment_entity->getShippingMethod()->getPlugin();
      if (!$shipment_plugin instanceof SwissPost) {
        continue;
      }

      $pdf_contents[] = $this->getLabel($shipment_entity);
    }

    if (empty($pdf_contents)) {
      return NULL;
    }
    elseif (count($pdf_contents) == 1) {
      return reset($pdf_contents);
    }
    else {
      return $this->mergePdfs($pdf_contents);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateLabelResponse(array $shipments) {
    $pdf = $this->generateLabels($shipments);

    $response = new Response($pdf, 200, [
      'Content-Type' => 'application/pdf',
      'Content-Length' => strlen($pdf),
    ]);

    $ids = \array_map(function (ShipmentInterface $shipment) {
      return $shipment->id();
    }, $shipments);

    $filename = 'SwissPostLabels-' . \implode('-', $ids) . '.pdf';
    $response->headers->set('Content-Disposition', $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename));
    return $response;
  }

  /**
   * Merges multiple PDF strings together.
   *
   * @param string[] $pdf_contents
   *   A list of PDFs to merge.
   *
   * @return string
   *   The combined PDF.
   */
  protected function mergePdfs(array $pdf_contents): string {
    $pdf = new Fpdi();

    foreach ($pdf_contents as $pdf_content) {

      $pdf_stream = StreamReader::createByString($pdf_content);
      $pdf->setSourceFile($pdf_stream);
      $template_id = $pdf->importPage(1);

      // Get the size of the imported page.
      $size = $pdf->getTemplateSize($template_id);

      // Create a page, landscape or portrait depending on the imported page
      // size.
      $pdf->AddPage($size['orientation'], [$size['width'], $size['height']]);

      $pdf->useTemplate($template_id);
    }

    return $pdf->Output('S');
  }

  /**
   * Builds the request data for a Barcode Label API request.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return array
   *   The built data as an array.
   */
  protected function buildRequestData(ShipmentInterface $shipment): array {
    $config = $this->configFactory->get('commerce_swiss_post.settings');
    $shipment_plugin = $shipment->getShippingMethod()->getPlugin();

    $store = $shipment->getOrder()->getStore();
    $store_address = $store->getAddress();
    $shipping_profile = $shipment->getShippingProfile();

    // Split up the street into street and house number.
    $street = NULL;
    $house_no = NULL;
    /** @var \Drupal\address\AddressInterface $shipping_address */
    $shipping_address = NULL;
    if ($shipping_profile && $shipping_profile->hasField('address') && !$shipping_profile->get('address')->isEmpty()) {
      $shipping_address = $shipping_profile->get('address')->get(0);
      $street = $shipping_address->getAddressLine1();
      if (preg_match('/^(.+) (\d+[a-z]*)$/', trim($street), $matches)) {
        $street = $matches[1];
        $house_no = $matches[2];
      }
    }

    // Verify that the shipping method is configured.
    if (empty($shipment_plugin->getConfiguration()['shipping_method'])) {
      throw new \RuntimeException('Shipping method ' . $shipment->getShippingMethod()->label() . ' is not configured');
    }

    $data = [
      // @todo: Only EN/FR/IT/DE supported. global setting? usually not known
      //   per client.
      'language' => 'EN',
      'frankingLicense' => $config->get('franking_license'),
      // @todo: Setting or rely on alter?
      'ppFranking' => FALSE,

      'customer' => [
        'name1' => $store->label(),
        'name2' => NULL,
        'street' => $store_address->getAddressLine1(),
        'zip' => $store_address->getPostalCode(),
        'city' => $store_address->getLocality(),
        'country' => $store_address->getCountryCode(),
        // @todo Support Logo settings?
        'logo' => NULL,
        'logoFormat' => NULL,
        'logoRotation' => 0,
        'logoAspectRatio' => NULL,
        'logoHorizontalAlign' => NULL,
        'logoVerticalAlign' => NULL,
        'domicilePostOffice' => NULL,
        'pobox' => NULL,
      ],
      'customerSystem' => NULL,
      // @todo Define configuration settings.
      'labelDefinition' => [
        'labelLayout' => $config->get('label_layout'),
        'printAddresses' => 'RECIPIENT_AND_CUSTOMER',
        'imageFileType' => 'PDF',
        'imageResolution' => 300,
        'printPreview' => $config->get('print_preview'),
      ],
      'sendingID' => NULL,
      'item' => [
        'notification' => [],
        'additionalData' => [],
        'itemID' => NULL,
        // @todo Use something by default, shipping ID, tokens, or just alter?
        'itemNumber' => NULL,
        'recipient' => [
          'postIdent' => NULL,
          'title' => NULL,
          'personallyAddressed' => FALSE,
          'name1' => $shipping_address ? $shipping_address->getFamilyName() : NULL,
          'firstName' => $shipping_address ? $shipping_address->getGivenName() : NULL,
          'name2' => $shipping_address && $shipping_address->getOrganization() ? $shipping_address->getOrganization() : NULL,
          'name3' => NULL,
          'addressSuffix' => NULL,
          'street' => $street,
          'houseNo' => $house_no,
          'floorNo' => NULL,
          'mailboxNo' => NULL,
          'zip' => $shipping_address ? $shipping_address->getPostalCode() : NULL,
          'city' => $shipping_address ? $shipping_address->getLocality() : NULL,
          'country' => $shipping_address ? $shipping_address->getCountryCode() : NULL,
          'phone' => NULL,
          'mobile' => NULL,
          'labelAddress' => NULL,
          'pobox' => NULL,
          'email' => NULL,
        ],
        'attributes' => [
          'przl' => [
            0 => $shipment_plugin->getConfiguration()['shipping_method'],
          ],
          'freeText' => NULL,
          'deliveryDate' => NULL,
          'parcelNo' => 0,
          'parcelTotal' => 0,
          'deliveryPlace' => NULL,
          'proClima' => FALSE,
          'weight' => $shipment->getWeight() ? $shipment->getWeight()
            ->convert(WeightUnit::GRAM)
            ->getNumber() : 0,
          'unnumbers' => NULL,
        ],
      ],
    ];

    $event = new ShipmentToBarcodeEvent($data, $shipment);
    $this->eventDispatcher->dispatch($event, ShipmentToBarcodeEvent::class);
    $data = $event->getBarcodeData();
    return $data;
  }

  /**
   * Returns the barcode label for a shipment.
   *
   * Either request it from the API and, if enabled, persist it or return
   * the local file.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return string
   *   The PDF content.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  protected function getLabel(ShipmentInterface $shipment): string {

    // If there is already a persisted file that exists, return it.
    if ($shipment->hasField('commerce_swisspost_barcode_label')) {
      if (!$shipment->get('commerce_swisspost_barcode_label')->isEmpty() && $shipment->get('commerce_swisspost_barcode_label')->entity) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $shipment->get('commerce_swisspost_barcode_label')->entity;
        if (\file_exists($file->getFileUri())) {
          return \file_get_contents($file->getFileUri());
        }
      }
    }

    $config = $this->configFactory->get('commerce_swiss_post.settings');

    $access_token = $this->accessTokenProvider->getAccessToken();

    $data = $this->buildRequestData($shipment);
    $url = $config->get('api_host') . '/api/barcode/v1/generateAddressLabel';

    try {
      $start = microtime(TRUE);

      $options = [
        'json' => $data,
        'headers' => [
          'Authorization' => 'Bearer ' . $access_token,
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
      ];

      $response = $this->client->request('POST', $url, $options);

      $body = (string) $response->getBody();

      $json = Json::decode($body);

      // Certain errors do not lead to a HTTP Error response, check for those.
      if (!empty($json['item']['errors'])) {
        throw new \Exception(t('@code item error returned by API: @error (Shipment #@shipment_id, Order #@order_id)', [
          '@code' => $json['item']['errors'][0]['code'],
          '@error' => $json['item']['errors'][0]['message'],
          '@shipment_id' => $shipment->id(),
          '@order_id' => $shipment->getOrderId(),
        ]));
      }

      // If the label is empty but there is no error, display the full info.
      if (empty($json['item']['label'][0])) {
        throw new \Exception(t('No Label returned by API: @response (Shipment #@shipment_id, Order #@order_id)', [
          '@error' => $body,
          '@shipment_id' => $shipment->id(),
          '@order_id' => $shipment->getOrderId(),
        ]));
      }

      $pdf_content = base64_decode($json['item']['label'][0]);

      $shipment->setTrackingCode($json['item']['identCode']);

      if ($shipment->hasField('commerce_swisspost_barcode_label')) {
        // The shipment entity has a file field, store the label as a file and
        // reference it.

        if (!$shipment->get('commerce_swisspost_barcode_label')
          ->offsetExists(0)) {
          $shipment->get('commerce_swisspost_barcode_label')
            ->appendItem();
        }

        $directory = $shipment->get('commerce_swisspost_barcode_label')
          ->get(0)
          ->getUploadLocation();
        $destination = $directory . '/SwissPostLabel-' . $shipment->id() . '.pdf';

        $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

        $this->fileSystem->saveData($pdf_content, $destination);

        $file = File::create([
          'uri' => $destination,
        ]);
        $file->setPermanent();
        $file->save();
        $shipment->set('commerce_swisspost_barcode_label', $file->id());
      }

      $shipment->save();

      $time = round((microtime(TRUE) - $start) * 1000, 2);

      \Drupal::logger('comerce_swiss_post')->notice('Generated barcode for shipment @id in @timems', [
        '@id' => $shipment->id(),
        '@time' => $time,
      ]);

      return $pdf_content;
    }
    catch (BadResponseException $e) {
      if ($e->getResponse()->getStatusCode() == 400) {
        $body = (string) $e->getResponse()->getBody();

        $json = Json::decode($body);
        if (\is_array($json) && isset($json[0])) {
          throw new \Exception((string) t('Incomplete/Invalid parameter returned from API for field @field with value @value: @error (Shipment #@shipment_id, Order #@order_id)', [
            '@field' => $json[0]['field'],
            '@error' => $json[0]['error'],
            '@value' => $json[0]['rejectedValue'],
            '@shipment_id' => $shipment->id(),
            '@order_id' => $shipment->getOrderId(),
          ]));
        }
      }
      throw new \Exception(t('@status_code error returned by API: @error (Shipment #@shipment_id, Order #@order_id)', [
        '@status_code' => $e->getResponse()->getStatusCode(),
        '@error' => trim(strip_tags((string) $e->getResponse()->getBody())),
        '@shipment_id' => $shipment->id(),
        '@order_id' => $shipment->getOrderId(),
      ]));
    }
  }

}
