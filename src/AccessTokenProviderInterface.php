<?php

namespace Drupal\commerce_swiss_post;

/**
 * Provides the Swiss Post Commerce API OAuth2 access token.
 */
interface AccessTokenProviderInterface {

  /**
   * Returns the access token.
   *
   * @param bool $force_new
   *   Force requesting a new access token even if the current one is still
   *   valid.
   *
   * @return string
   *   The access token, if one could be requested. An exception is thrown
   *   if not.
   *
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function getAccessToken(bool $force_new = FALSE): string;

}
