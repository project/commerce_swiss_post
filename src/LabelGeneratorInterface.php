<?php

namespace Drupal\commerce_swiss_post;

/**
 * Interface for the Label Generator service.
 */
interface LabelGeneratorInterface {

  /**
   * Generate barcodes for shipments as a PDF.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipment_entities
   *   The commerce_shipment entities to generate labels for.
   *
   * @return string
   *   The PDF content.
   */
  public function generateLabels(array $shipment_entities);

  /**
   * Generate barcode labels for shipments as a response.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments
   *   The commerce_shipment entities to generate labels for.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function generateLabelResponse(array $shipments);

}
