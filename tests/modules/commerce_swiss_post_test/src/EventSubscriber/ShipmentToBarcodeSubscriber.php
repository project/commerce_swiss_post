<?php

namespace Drupal\commerce_swiss_post_test\EventSubscriber;

use Drupal\commerce_swiss_post\Events\ShipmentToBarcodeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ShipmentToBarcodeSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ShipmentToBarcodeEvent::class => 'alterBarcodeData',
    ];
  }

  /**
   * Test changes to the barcode data.
   *
   * @param \Drupal\commerce_swiss_post\Events\ShipmentToBarcodeEvent $event
   *   The event.
   */
  public function alterBarcodeData(ShipmentToBarcodeEvent $event) {

    $shipping_profile = $event->getShipment()->getShippingProfile();
    /** @var \Drupal\address\AddressInterface $shipping_address */
    if ($shipping_profile && $shipping_profile->hasField('address') && !$shipping_profile->get('address')->isEmpty() && $shipping_address = $shipping_profile->get('address')->get(0)) {

      $barcode_data = $event->getBarcodeData();

      // If there is no house number but address line 2 matches the house number
      // format then use that as house number.
      if (empty($barcode_data['item']['recipient']['houseNo'])) {
        $address_line2 = $shipping_address->getAddressLine2();
        if (preg_match('/^(\d+[a-z]*)$/', trim($address_line2), $matches)) {
          $barcode_data['item']['recipient']['houseNo'] = trim($address_line2);
        }
      }
    }

    // Look for a c/o in address line 2.
    if ($shipping_address->getAddressLine2() && \preg_match('/^c\/o.+$/i', trim($shipping_address->getAddressLine2()))) {
      $barcode_data['item']['recipient']['name3'] = trim($shipping_address->getAddressLine2());
    }

    // Look for a c/o in address line 1.
    if ($shipping_address->getAddressLine1() && \preg_match('/^c\/o.+/i', trim($shipping_address->getAddressLine1()))) {
      $barcode_data['item']['recipient']['name3'] = $shipping_address->getAddressLine1();

      // If address line 2 is not empty, use it as street.
      if ($shipping_address->getAddressLine2()) {
        $street = $shipping_address->getAddressLine2();
        $house_no = NULL;
        if (preg_match('/^(.+) (\d+[a-z]*)$/', trim($street), $matches)) {
          $street = $matches[1];
          $house_no = $matches[2];
        }

        $barcode_data['item']['recipient']['street'] = $street;
        $barcode_data['item']['recipient']['houseNo'] = $house_no;
      }
    }

    $event->setBarcodeData($barcode_data);
  }
}
