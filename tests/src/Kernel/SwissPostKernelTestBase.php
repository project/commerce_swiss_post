<?php

namespace Drupal\Tests\commerce_swiss_post\Kernel;

use Drupal\Tests\commerce_shipping\Kernel\ShippingKernelTestBase;

/**
 * Base class for Swiss Post kernel tests.
 */
abstract class SwissPostKernelTestBase extends ShippingKernelTestBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The payment gateway config entity.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $paymentGateway;

  protected static $modules = [
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce_number_pattern',
    'commerce_order',
    'commerce_payment',
    'commerce_swiss_post',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');

    $this->installConfig(['commerce_swiss_post']);

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Create an order.
    $this->order = $this->entityTypeManager->getStorage('commerce_order')->create([
      'type' => 'default',
      'order_number' => '1',
      'store_id' => $this->store->id(),
      'state' => 'draft',
    ]);
    $this->order->save();

    $this->store->set('address', [
      'country_code' => 'CH',
      'address_line1' => 'Store street',
      'postal_code' => 8048,
      'locality' => 'Zürich',
    ]);
    $this->store->save();
  }

}
