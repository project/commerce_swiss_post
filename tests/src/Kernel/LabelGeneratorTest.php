<?php

namespace Drupal\Tests\commerce_swiss_post\Kernel;

use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShipmentType;
use Drupal\commerce_shipping\Entity\ShippingMethod;
use Drupal\commerce_swiss_post\AccessTokenProviderInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DrupalKernel;
use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;
use Drupal\profile\Entity\Profile;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\commerce_swiss_post\LabelGenerator
 *
 * @group commerce_swiss_post
 */
class LabelGeneratorTest extends SwissPostKernelTestBase {

  /**
   * Example PDF content.
   *
   * @var string
   */
  protected $pdfContent = 'I am a Barcode PDF';

  /**
   * Example tracking code.
   *
   * @var string
   */
  protected $expectedTrackingCode = '996006172900000001';

  /**
   * The mocked HTTP client object.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $httpClient;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce_number_pattern',
    'commerce_order',
    'commerce_payment',
    'commerce_shipping',
    'physical',
    'field',
    'file',
    'commerce_order',
    'commerce_swiss_post',
    'commerce_swiss_post_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');

    $trait_id = 'commerce_swiss_post_barcode_label';
    $shipment_type = ShipmentType::load('default');
    $shipment_type->setTraits([$trait_id]);
    $shipment_type->save();

    $trait_manager = \Drupal::service('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance($trait_id);
    $trait_manager->installTrait($trait, 'commerce_shipment', $shipment_type->id());

    $request = Request::create('/');
    $site_path = DrupalKernel::findSitePath($request);
    $this->setSetting('file_private_path', $site_path . '/private');

    $this->config('commerce_swiss_post.settings')
      ->set('franking_license', '00001111')
      ->save();

    $access_token_provider = $this->prophesize(AccessTokenProviderInterface::class);
    $access_token_provider->getAccessToken()->willReturn('super-secure-token');

    $this->container->set('commerce_swiss_post.access_token_provider', $access_token_provider->reveal());
    $this->container->set('commerce_swiss_post.label_generator', NULL);

    $this->httpClient = $this->prophesize(ClientInterface::class);
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $container->register('stream_wrapper.private', 'Drupal\Core\StreamWrapper\PrivateStream')
      ->addTag('stream_wrapper', ['scheme' => 'private']);
  }

  /**
   * @covers ::generateLabels
   * @covers ::buildRequestData
   */
  public function testPersonalLabel() {
    $request_data = $this->getRequestData();

    $this->mockSuccessResponse($request_data);

    $shipment = $this->createShipment();

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));

    $shipment = Shipment::load($shipment->id());
    $this->assertEquals($this->expectedTrackingCode, $shipment->getTrackingCode());

    // Call it a second time, ensure the API is only called once.
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));

    // Run cron and check that the newly created file is not deleted.
    \commerce_swiss_post_cron();
    $this->assertNotNull($shipment->get('commerce_swisspost_barcode_label')->first());

    // Make the file older than 180 days, run cron, and check that is deleted.
    $file = $shipment->get('commerce_swisspost_barcode_label')->entity;
    $file->set('created', \Drupal::time()->getRequestTime() - 19552000)->save();
    \commerce_swiss_post_cron();
    $this->assertNull($shipment->get('commerce_swisspost_barcode_label')->first());
  }

  /**
   * Test organisation label generation.
   */
  public function testOrganisationLabel() {
    $request_data = $this->getRequestData();
    $request_data['item']['recipient']['name2'] = 'Llama Inc.';

    $this->mockSuccessResponse($request_data);
    $shipment = $this->createShipment('PRI', ['organization' => 'Llama Inc.']);

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));

    // Assert that the file was created.
    $file_uri = $shipment->get('commerce_swisspost_barcode_label')->entity->getFileUri();
    $this->assertEquals($this->pdfContent, \file_get_contents($file_uri));
  }

  /**
   * Tests the alter hook.
   */
  public function testRequestDataEvent() {
    $request_data = $this->getRequestData();

    $this->mockSuccessResponse($request_data);
    $shipment = $this->createShipment('PRI', [
      'address_line1' => 'Example Street',
      'address_line2' => '77',
    ]);

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));

    // Assert that the file was created.
    $file_uri = $shipment->get('commerce_swisspost_barcode_label')->entity->getFileUri();
    $this->assertEquals($this->pdfContent, \file_get_contents($file_uri));
  }

  /**
   * Tests the alter hook.
   */
  public function testRequestDataEventCo1() {
    $request_data = $this->getRequestData();
    $request_data['item']['recipient']['name3'] = 'c/o John Snow';

    $this->mockSuccessResponse($request_data);
    $shipment = $this->createShipment('PRI', [
      'address_line1' => 'c/o John Snow',
      'address_line2' => 'Example Street 77',
    ]);

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));

    // Assert that the file was created.
    $file_uri = $shipment->get('commerce_swisspost_barcode_label')->entity->getFileUri();
    $this->assertEquals($this->pdfContent, \file_get_contents($file_uri));
  }

  /**
   * Tests the alter hook.
   */
  public function testRequestDataEventCo2() {
    $request_data = $this->getRequestData();
    $request_data['item']['recipient']['name3'] = 'C/o John Snow';

    $this->mockSuccessResponse($request_data);
    $shipment = $this->createShipment('PRI', [
      'address_line1' => 'Example Street 77',
      'address_line2' => 'C/o John Snow',
    ]);

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));

    // Assert that the file was created.
    $file_uri = $shipment->get('commerce_swisspost_barcode_label')->entity->getFileUri();
    $this->assertEquals($this->pdfContent, \file_get_contents($file_uri));
  }

  /**
   * @covers ::generateLabels
   * @covers ::buildRequestData
   */
  public function testPrintPreview() {
    $this->config('commerce_swiss_post.settings')
      ->set('print_preview', TRUE)
      ->save();

    $request_data = $this->getRequestData();
    $request_data['labelDefinition']['printPreview'] = TRUE;

    $this->mockSuccessResponse($request_data);
    $this->container->set('http_client', $this->httpClient->reveal());

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $shipment = $this->createShipment();
    $this->assertEquals($this->pdfContent, $label_generator->generateLabels([$shipment]));
  }

  /**
   * @covers ::generateLabels
   * @covers ::buildRequestData
   */
  public function testMissingShippingMethodConfiguration() {

    $access_token_provider = $this->prophesize(AccessTokenProviderInterface::class);
    $access_token_provider->getAccessToken()->willReturn('super-secure-token');
    $this->container->set('commerce_swiss_post.access_token_provider', $access_token_provider->reveal());
    $this->container->set('commerce_swiss_post.label_generator', NULL);

    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = Shipment::create([
      'type' => 'default',
      'state' => 'ready',
      'title' => 'Shipment',
      'order_id' => $this->order->id(),
    ]);
    $shipment->setWeight(new Weight('4.9', WeightUnit::KILOGRAM));

    /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method */
    $shipping_method = ShippingMethod::create([
      'name' => 'Example Method',
      'status' => 1,
      'plugin' => [
        'target_plugin_id' => 'swiss_post',
        'target_plugin_configuration' => [
          'shipping_method' => '',
        ],
      ],
    ]);
    $shipping_method->save();
    $shipment->setShippingMethod($shipping_method);

    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'CH',
        'address_line1' => 'Example Street 77',
        'postal_code' => 9999,
        'locality' => 'Example',
        'given_name' => 'Example',
        'family_name' => 'Person',
      ],
    ]);
    $profile->save();
    $shipment->setShippingProfile($profile);

    $shipment->save();

    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage('Shipping method Example Method is not configured');

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $label_generator->generateLabels([$shipment]);
  }

  /**
   * @covers ::generateLabels
   * @covers ::buildRequestData
   */
  public function testItemError() {
    $request_data = $this->getRequestData();
    $request_data['item']['attributes']['weight'] = '40000';

    $this->mockSuccessResponse($request_data);

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $shipment = $this->createShipment();
    $shipment->setWeight(new Weight('40', WeightUnit::KILOGRAM));
    $shipment->save();

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('E2036 item error returned by API: The declaration of weight must comprise no more than 5 digits and must not exceed 30,000 grams (e.g. 29500). (Shipment #1, Order #1)');

    $label_generator->generateLabels([$shipment]);
  }

  /**
   * @covers ::generateLabels
   * @covers ::buildRequestData
   */
  public function testServiceError() {
    $request_data = $this->getRequestData();

    $expected_options = [
      'json' => $request_data,
      'headers' => [
        'Authorization' => 'Bearer super-secure-token',
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
    ];

    $response_data = [
      [
        'field' => 'item.recipient.houseNo',
        'rejectedValue' => 'abc',
        'error' => 'IAmError',
      ],
    ];

    $request = new \GuzzleHttp\Psr7\Request('POST', 'https://wedec.post.ch/api/barcode/v1/generateAddressLabel');
    $response = new Response(400, [], Json::encode($response_data));

    $exception = new BadResponseException('Error', $request, $response);
    $this->httpClient->request('POST', 'https://wedec.post.ch/api/barcode/v1/generateAddressLabel', Argument::is($expected_options))->willThrow($exception);
    $this->container->set('http_client', $this->httpClient->reveal());

    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $this->createShipment();

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Incomplete/Invalid parameter returned from API for field item.recipient.houseNo with value abc: IAmError (Shipment #1, Order #1)');

    $label_generator = $this->container->get('commerce_swiss_post.label_generator');
    $label_generator->generateLabels([$shipment]);
  }

  /**
   * Returns example expected request data.
   *
   * @return array
   *   The expected request data.
   */
  public function getRequestData() {
    return [
      'language' => 'EN',
      'frankingLicense' => '00001111',
      'ppFranking' => FALSE,

      'customer' => [
        'name1' => 'Default store',
        'name2' => NULL,
        'street' => 'Store street',
        'zip' => '8048',
        'city' => 'Zürich',
        'country' => 'CH',
        'logo' => NULL,
        'logoFormat' => NULL,
        'logoRotation' => 0,
        'logoAspectRatio' => NULL,
        'logoHorizontalAlign' => NULL,
        'logoVerticalAlign' => NULL,
        'domicilePostOffice' => NULL,
        'pobox' => NULL,
      ],
      'customerSystem' => NULL,
      'labelDefinition' => [
        'labelLayout' => 'A6',
        'printAddresses' => 'RECIPIENT_AND_CUSTOMER',
        'imageFileType' => 'PDF',
        'imageResolution' => 300,
        'printPreview' => FALSE,
      ],
      'sendingID' => NULL,
      'item' => [
        'notification' => [],
        'additionalData' => [],
        'itemID' => NULL,
        'itemNumber' => NULL,
        'recipient' => [
          'postIdent' => NULL,
          'title' => NULL,
          'personallyAddressed' => FALSE,
          'name1' => 'Person',
          'firstName' => 'Example',
          'name2' => NULL,
          'name3' => NULL,
          'addressSuffix' => NULL,
          'street' => 'Example Street',
          'houseNo' => '77',
          'floorNo' => NULL,
          'mailboxNo' => NULL,
          'zip' => '9999',
          'city' => 'Example',
          'country' => 'CH',
          'phone' => NULL,
          'mobile' => NULL,
          'labelAddress' => NULL,
          'pobox' => NULL,
          'email' => NULL,
        ],
        'attributes' => [
          'przl' => [
            0 => 'PRI',
          ],
          'freeText' => NULL,
          'deliveryDate' => NULL,
          'parcelNo' => 0,
          'parcelTotal' => 0,
          'deliveryPlace' => NULL,
          'proClima' => FALSE,
          'weight' => '4900',
          'unnumbers' => NULL,
        ],
      ],
    ];
  }

  /**
   * Mocks the expected HTTP API request.
   *
   * @param array $request_data
   *   The expected request data sent to the service.
   */
  protected function mockSuccessResponse(array $request_data): void {
    $expected_options = [
      'json' => $request_data,
      'headers' => [
        'Authorization' => 'Bearer super-secure-token',
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
    ];

    if ($request_data['item']['attributes']['weight'] > 30000) {
      $response_data = [
        'item' => [
          'errors' => [
            [
              'code' => 'E2036',
              'message' => 'The declaration of weight must comprise no more than 5 digits and must not exceed 30,000 grams (e.g. 29500).'
            ],
          ],
          'label' => [
            0 => NULL,
          ],
        ],
      ];
    }
    else {
      $response_data = [
        'item' => [
          'identCode' => $this->expectedTrackingCode,
          'label' => [
            0 => \base64_encode($this->pdfContent),
          ],
        ],
      ];
    }

    $response = new Response(200, [], Json::encode($response_data));

    $this->httpClient->request('POST', 'https://wedec.post.ch/api/barcode/v1/generateAddressLabel', Argument::is($expected_options))
      ->willReturn($response)
      ->shouldBeCalledTimes(1);
    $this->container->set('http_client', $this->httpClient->reveal());
  }

  /**
   * Creates the shipment with shipping method and profile.
   *
   * @param string $swisspost_shipping_method
   *   The shipping method, defaults to PRI.
   * @param array $address
   *   Non-default address field values.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createShipment(string $swisspost_shipping_method = 'PRI', array $address = []): ShipmentInterface {
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = Shipment::create([
      'type' => 'default',
      'state' => 'ready',
      'title' => 'Shipment',
      'order_id' => $this->order->id(),
    ]);
    $shipment->setWeight(new Weight('4.9', WeightUnit::KILOGRAM));

    /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method */
    $shipping_method = ShippingMethod::create([
      'name' => $this->randomString(),
      'status' => 1,
      'plugin' => [
        'target_plugin_id' => 'swiss_post',
        'target_plugin_configuration' => [
          'shipping_method' => $swisspost_shipping_method,
        ],
      ],
    ]);
    $shipping_method->save();
    $shipment->setShippingMethod($shipping_method);

    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = Profile::create([
      'type' => 'customer',
      'address' => $address + [
        'country_code' => 'CH',
        'address_line1' => 'Example Street 77',
        'postal_code' => 9999,
        'locality' => 'Example',
        'given_name' => 'Example',
        'family_name' => 'Person',
      ],
    ]);
    $profile->save();
    $shipment->setShippingProfile($profile);

    $shipment->save();
    return $shipment;
  }

}
