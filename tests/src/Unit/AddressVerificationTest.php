<?php

namespace Drupal\Tests\commerce_swiss_post\Unit;

use Drupal\commerce_swiss_post\AddressVerification;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\commerce_swiss_post\AddressVerification
 *
 * @group commerce
 * @group commerce_swiss_post
 * @group commerce_swiss_post_unit
 */
class AddressVerificationTest extends UnitTestCase {

  /**
   * @covers ::verifyAddress
   *
   * @dataProvider providerAddressData
   */
  public function testSetEventDetailViews($address, $expected) {
    $matches = AddressVerification::extractStreetParts($address);
    $this->assertSame($expected, $matches, 'Failed on: ' . $address);
  }

  /**
   * List of supported event data.
   *
   * @return array
   *   Examples of event data structure by event types.
   */
  public function providerAddressData() {
    return [
      [
        '15, Foo bar street',
        [
          'street' => 'Foo bar street',
          'house_no' => '15',
          'house_no_addition' => '',
        ],
      ],
      [
        '101, Foo bar street',
        [
          'street' => 'Foo bar street',
          'house_no' => '101',
          'house_no_addition' => '',
        ],
      ],
      [
        '15a,Foo bar street',
        [
          'street' => 'Foo bar street',
          'house_no' => '15',
          'house_no_addition' => 'a',
        ],
      ],
      [
        'Foo. bar street, 15',
        [
          'street' => 'Foo. bar street',
          'house_no' => '15',
          'house_no_addition' => '',
        ],
      ],
      [
        'Foo. bar street, 101',
        [
          'street' => 'Foo. bar street',
          'house_no' => '101',
          'house_no_addition' => '',
        ],
      ],
      [
        'Foo. bar street 55',
        [
          'street' => 'Foo. bar street',
          'house_no' => '55',
          'house_no_addition' => '',
        ],
      ],
      [
        'Foobarstreet, 55',
        [
          'street' => 'Foobarstreet',
          'house_no' => '55',
          'house_no_addition' => '',
        ],
      ],
      [
        'Foo. bar street, 15b',
        [
          'street' => 'Foo. bar street',
          'house_no' => '15',
          'house_no_addition' => 'b',
        ],
      ],
      [
        'Foo bar street, 4/8',
        [
          'street' => 'Foo bar street',
          'house_no' => '4/8',
          'house_no_addition' => '',
        ],
      ],
      [
        '4/8, Foo bar street',
        [
          'street' => 'Foo bar street',
          'house_no' => '4/8',
          'house_no_addition' => '',
        ],
      ],
      [
        '4/8 Foo bar street',
        [
          'street' => 'Foo bar street',
          'house_no' => '4/8',
          'house_no_addition' => '',
        ],
      ],
      [
        'Foobar street without number C',
        [
          'street' => 'Foobar street without number C',
          'house_no' => '',
          'house_no_addition' => '',
        ],
      ],
    ];
  }

}
