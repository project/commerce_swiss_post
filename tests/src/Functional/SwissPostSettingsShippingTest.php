<?php

namespace Drupal\Tests\commerce_swiss_post\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Swiss Post settings UI.
 *
 * @group commerce_swiss_post
 */
class SwissPostSettingsShippingTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['commerce_swiss_post', 'commerce_shipping'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  public function testSettings() {
    $this->drupalGet('/admin/config/services/swiss-post-settings');
    $this->assertSession()->statusCodeEquals(403);

    $account = $this->drupalCreateUser(['administer site configuration', 'access administration pages']);
    $this->drupalLogin($account);

    $this->drupalGet('/admin/config/services/swiss-post-settings');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'api_host' => 'api_host_test_value',
      'client_id' => 'client_id_test_value',
      'client_secret' => 'client_secret_test_value',
      'franking_license' => 'franking_license_test_value',
      'label_layout' => 'a5',
      'print_preview' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->drupalGet('/admin/config/services');
    $this->assertSession()->statusCodeEquals(200);
    $this->clickLink('Swiss Post settings');
    $this->assertSession()->fieldValueEquals('api_host', 'api_host_test_value');
    $this->assertSession()->fieldValueEquals('client_id', 'client_id_test_value');
    $this->assertSession()->fieldValueEquals('client_secret', 'client_secret_test_value');
    $this->assertSession()->fieldValueEquals('franking_license', 'franking_license_test_value');
    $this->assertSession()->fieldValueEquals('label_layout', 'a5');
    $this->assertSession()->checkboxChecked('print_preview');
  }

}
