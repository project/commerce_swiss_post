<?php

namespace Drupal\Tests\commerce_swiss_post\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Swiss Post settings UI without the shipping module.
 *
 * @group commerce_swiss_post
 */
class SwissPostSettingsNoShippingTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['commerce_swiss_post'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  public function testSettings() {
    $account = $this->drupalCreateUser(['administer site configuration', 'access administration pages']);
    $this->drupalLogin($account);

    $this->drupalGet('/admin/config/services');
    $this->assertSession()->statusCodeEquals(200);
    $this->clickLink('Swiss Post settings');
    $this->assertSession()->fieldNotExists('api_host');
    $this->assertSession()->fieldNotExists('client_id');
    $this->assertSession()->fieldNotExists('client_secret');
    $this->assertSession()->fieldNotExists('franking_license');
    $this->assertSession()->fieldNotExists('label_layout');
    $this->assertSession()->fieldNotExists('print_preview');

    $edit = [
      'aws_host' => 'the-host',
      'aws_username' => 'myuser',
      'aws_password' => 'secret',
      'aws_log_level' => 2,
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->fieldValueEquals('aws_host', 'the-host');
    $this->assertSession()->fieldValueEquals('aws_username', 'myuser');
    $this->assertSession()->fieldValueEquals('aws_password', 'secret');
    $this->assertSession()->fieldValueEquals('aws_log_level', 2);


  }

}
